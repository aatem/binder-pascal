# Jupyter environment for pari-gp + arb

Based on [repo2docker](https://repo2docker.readthedocs.io/en/latest/usage.html).

## Development

```
jupyter-repo2docker --debug .
```

This can be run under conda
```
conda create -n binder jupyter jupyter-repo2docker -c conda-forge
conda activate binder
```
